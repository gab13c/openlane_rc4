# User config
set ::env(DESIGN_NAME) nm
set ::env(SYNTH_STRATEGY) 2
set ::env(SYNTH_MAX_FANOUT) 4


# Change if needed
set ::env(VERILOG_FILES) [glob $::env(DESIGN_DIR)/src/*.v]

# Fill this
set ::env(CLOCK_PERIOD) "600.00"
set ::env(CLOCK_PORT) "G_CLK"

#Floorplanning

#Placement
set ::env(PL_TIME_DRIVEN) 1


set filename $::env(DESIGN_DIR)/$::env(PDK)_$::env(STD_CELL_LIBRARY)_config.tcl
if { [file exists $filename] == 1} {
	source $filename
}

